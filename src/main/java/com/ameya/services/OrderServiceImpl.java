package com.ameya.services;

import com.ameya.daos.OrderDao;

public class OrderServiceImpl implements OrderService {
	
	private OrderDao dao;
	public OrderServiceImpl(OrderDao dao) {
		this.dao=dao;
	}
static void test() {
	System.out.println("Test");
}
	@Override
	public int findTheGreatestFromAllData() {
		int [] data=dao.getAllData();
		int greatest=Integer.MIN_VALUE;
		if(data.length==0) {
			return 0;
		}
		for(int value : data) {
			if(value>greatest) {
				greatest=value;
			}
		}
		return greatest;
	}

}
