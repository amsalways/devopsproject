package com.ameya.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import com.ameya.daos.OrderDao;
import com.ameya.services.OrderService;
import com.ameya.services.OrderServiceImpl;

public class MyServiceTest {

	OrderDao dao;
	OrderService service;
	int arr1[]=null;
	int arr2[]=null;
	int arr3[]=null;
	
	@BeforeClass
	public static void setUpOnce() {
		System.out.println("This is set up before class");
	}
	@AfterClass
	public static void tearDownClass() {
		System.out.println("This is tear down after class");
	}
	@Before
	public void setUpTest() {
		arr1= new int[3];
		arr1[0]=24;arr1[1]=15;arr1[2]=3;
		arr2=new int[1];
		arr2[0]=15;
		arr3=new int[3];
		arr3[0]=-3;arr3[1]=-2;arr3[2]=-5;
		dao=Mockito.mock(OrderDao.class);
		service=new OrderServiceImpl(dao);
		System.out.println("++++ Test Data Set Up ++++");
	}
	
	@After
	public void tearDownTest() {
		service=null;
		arr1=null;
		arr2=null;
		arr3=null;
		System.out.println("++++ Test Data Cleaned Up ++++");
	}

	@Test
	public void testFindTheGreatestFromAllData(){
		when(dao.getAllData()).thenReturn(arr1);
		assertEquals(24,service.findTheGreatestFromAllData());
		//assertEquals(24,OrderService.test()); -->static methods also can be tested
	}
	@Test
	public void testFindTheGreatestFromAllData_ForOneValue() {
		when(dao.getAllData()).thenReturn(arr2);
		assertEquals(15,service.findTheGreatestFromAllData());
	}
	@Test
	public void testFindTheGreatestFromAllData_NoValues() {
		when(dao.getAllData()).thenReturn(new int[] {});
		assertEquals(0,service.findTheGreatestFromAllData());
	}
	@Test
	public void testFindTheGreatestFromAllData_NegativeValues() {
		when(dao.getAllData()).thenReturn(arr3);
		assertEquals(-2,service.findTheGreatestFromAllData());
	}
}
